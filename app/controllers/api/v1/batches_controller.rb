require 'nokogiri'
require 'open-uri'

module Api
  module V1
    class BatchesController < ApiController

			def index
				xml = Nokogiri::XML(open("http://localhost:3333"))


				@examples = xml.search('example').map do |example|
				  %w[id name param-1 param-2 some-code some-price-param].each_with_object({}) do |n, o|
				    o[n] = example.at(n).text
				  end
				end
				render json: @examples

			end

		end
	end
end

